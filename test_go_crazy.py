import torch as tr
import numpy as np
import pims
from path import Path
from neural_wrappers.utilities import resize_batch
from matplotlib.cm import inferno
from Mihlib import plot_image, show_plots, saveImage, npGetInfo, figure_set_size
import matplotlib.pyplot as plt
from inverse_warp import inverse_warp
from PIL import Image

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def doWarp(image, depth, pose, K, K_inv):
	img = inverse_warp(image.unsqueeze(0), depth.unsqueeze(0), pose.unsqueeze(0), K, K_inv)
	npImg = img[0].detach().cpu().numpy().transpose(1, 2, 0)
	npImg = np.uint8(((npImg * 0.5) + 0.5) * 255)
	return img, npImg

def minMaxNormalize(x):
	x = np.float32(x)
	Min, Max = np.min(x), np.max(x)
	return (x - Min) / (Max - Min)

def getPose(R, t):
	return tr.FloatTensor([*t, *R]).to(device)

def getFrames(video, i):
	frames = resize_batch(np.float32(video[i : i + 100]), (480, 854, 3)) / 255
	npImg = np.float32((frames - 0.5) / 0.5)
	npImg = np.transpose(npImg, (0, 3, 1, 2))
	return frames, npImg

@tr.no_grad()
def test_go_crazy(model, video_path, startIndex):
	tr.set_printoptions(precision=3)
	video = pims.Video(video_path)

	K = np.array([[459.574375, 0, 222.8240625], [0, 378.28770136, 85.68987012], [0, 0, 1]], dtype=np.float32)
	K_inv = tr.from_numpy(np.linalg.inv(K)).unsqueeze(0).to(device)
	K = tr.from_numpy(K).unsqueeze(0).to(device)

	N = len(video) - (len(video) // 100)
	frames, npFrames = getFrames(video, startIndex)
	trFrames = tr.from_numpy(npFrames).to(device)

	# Generating new views at random
	thisFrame = trFrames[0 : 1]
	trDisp, explainability_mask, pose = model.forward((thisFrame, nextFrames))
	trDepth = 1 / trDisp[0][0]
	N = 200
	startRot, startTrans = -((np.random.rand(6)) / 10).reshape((2, 3))
	endRot, endTrans = -startRot, -startTrans
	rotRanges = (startRot+(np.expand_dims(np.arange(N), axis=-1) @ np.expand_dims((endRot-startRot)/N, axis=0)))
	transRanges = (startTrans+(np.expand_dims(np.arange(N), axis=-1) @ np.expand_dims((endTrans-startTrans)/N, axis=0)))
	print(rotRanges.shape, transRanges.shape)
	for i in range(N):
		R, t = rotRanges[i], transRanges[i]
		print(i, "/", N, R, t)
		img_new, npImg_new = doWarp(thisFrame[0], trDepth[0], getPose(R, t), K, K_inv)
		npDisp_new = model.disp_net.forward(img_new)[0][0][0].detach().cpu().numpy()

		plot_image(frames[0], axis=(2, 2, 1), new_figure=False, show_axis=False)
		plot_image(npDisp[0], axis=(2, 2, 3), new_figure=False, show_axis=False, cmap="inferno")
		Str = "R:[%2.2f,%2.2f,%2.2f]\nT:[%2.2f,%2.2f,%2.2f]" % (R[0], R[1], R[2], t[0], t[1], t[2])
		plot_image(npImg_new, axis=(2, 2, 2), new_figure=False, show_axis=False, title=Str)
		plot_image(npDisp_new, axis=(2, 2, 4), new_figure=False, show_axis=False, cmap="inferno")
		figure_set_size((14, 8))
		plt.savefig("res/%d.png" % (i))
		plt.gcf().clear()
		plt.clf()
		# show_plots()

	## Plot for pose/warping error as we go further from learned frames
	# thisFrame = trFrames[0 : 1]
	# resErrors, resPoses = [], []
	# for i in range(1, 95):
	# 	# nextFrames = trFrames[i : i + 4].unsqueeze(1)
	# 	nextFrames = tr.cat([trFrames[i : i+1], trFrames[i : i+1], trFrames[i : i+1], trFrames[i : i+1]]).unsqueeze(1)

	# 	trDisp, explainability_mask, pose = model.forward((thisFrame, nextFrames))
	# 	trDepth = 1 / trDisp[0][0]
	# 	npDisp = trDisp[0][0].detach().cpu().numpy()
	# 	npDepth = 1 / npDisp

	# 	warpedImages = [doWarp(nextFrames[i][0], trDepth[0], pose[0][i], K, K_inv)[0] for i in range(len(nextFrames))]
	# 	npWarpedImages = tr.cat(warpedImages, dim=0).detach().cpu().numpy()
	# 	# plot_image(frames[0], axis=(1, 2, 1), new_figure=True, show_axis=False)
	# 	# plot_image(npWarpedImages[1], axis=(1, 2, 2), new_figure=False, show_axis=False)
	# 	# show_plots()
	# 	errors = np.sum(np.abs(npWarpedImages - npFrames[0 : 1]), axis=(1, 2, 3))
	# 	poseErrors = tr.abs(pose[0]).sum(dim=1).detach().cpu().numpy()
	# 	resErrors.append(errors[1])
	# 	resPoses.append(poseErrors[1])
	# 	print(i, errors, poseErrors)

	# plt.plot(np.arange(len(resErrors)), resErrors, label="Warping error")
	# plt.xlabel("time")
	# plt.ylabel("error")
	# plt.legend()
	# plt.figure()
	# plt.plot(np.arange(len(resPoses)), resPoses, label="Relative poses")
	# plt.xlabel("time")
	# plt.ylabel("error")
	# plt.legend()
	# plt.show()

	## Generating forward and backward warps towards same position
	# def generateWarp(model, npFrames, K, K_inv, i, j):
	# 	trFrames = tr.from_numpy(npFrames).to(device)
	# 	thisFrame = trFrames[i : i + 1]
	# 	nextFrames = tr.cat([trFrames[j : j+1], trFrames[j : j+1], trFrames[j : j+1], trFrames[j : j+1]]).unsqueeze(1)

	# 	trDisp, explainability_mask, pose = model.forward((thisFrame, nextFrames))
	# 	trDepth = 1 / trDisp[0][0]
	# 	npDisp = trDisp[0][0].detach().cpu().numpy()
	# 	npDepth = 1 / npDisp

	# 	warpedImages = [doWarp(nextFrames[i][0], trDepth[0], pose[0][i], K, K_inv) for i in range(len(nextFrames))]
	# 	npWarpedImages = []
	# 	for k in range(len(warpedImages)):
	# 		npWarpedImages.append(warpedImages[k][1])
	# 		warpedImages[k] = warpedImages[k][0]
	# 	npWarpedImages = np.array(npWarpedImages)
	# 	warpedImages = tr.cat(warpedImages, dim=0).detach().cpu().numpy()
	# 	errors = np.sum(np.abs(warpedImages - npFrames[i : i+1]), axis=(1, 2, 3))

	# 	# print(errors)
	# 	return npWarpedImages[np.argmin(errors)], warpedImages[np.argmin(errors)], npDepth, np.min(errors)

	# fwdErrors, backErrors, newErrors, newErrors2 = [], [], [], []
	# fwdOcclusions, backOcclusions, newOcclusions = [], [], []
	# for i in range(82):
	# 	start, gen, end = i, i + 8, i + 16
	# 	npFwdImg, trFwdImg, fwdDph, fwdError = generateWarp(model, npFrames, K, K_inv, start, gen)
	# 	npBackImg, trBackImg, backDph, backError = generateWarp(model, npFrames, K, K_inv, end, gen)
	# 	whereFwdZeros = np.where(trFwdImg==0)
	# 	whereBackZeros = np.where(trBackImg==0)

	# 	fwdErrors.append(fwdError)
	# 	fwdOcclusions.append(len(whereFwdZeros[0]))
	# 	backErrors.append(backError)
	# 	backOcclusions.append(len(whereBackZeros[0]))

	# 	# plot_image(frames[start], axis=(2, 4, 1), new_figure=False, show_axis=False)
	# 	# plot_image(frames[end], axis=(2, 4, 2), new_figure=False, show_axis=False)

	# 	plot_image(frames[gen], axis=(1, 5, 1), new_figure=False, show_axis=False)
	# 	plot_image(npFwdImg, axis=(1, 5, 2), new_figure=False, show_axis=False, \
	# 		title="Forward\nError: %d\nNum occlusions: %d" % (fwdError, len(whereFwdZeros[0])))
	# 	plot_image(npBackImg, axis=(1, 5, 3), new_figure=False, show_axis=False, \
	# 		title="Backward\nError: %d\nNum occlusions: %d" % (backError, len(whereBackZeros[0])))

	# 	newImg = np.zeros(trFwdImg.shape)
	# 	newImg2 = np.copy(newImg)
	# 	newImg[whereFwdZeros] = trBackImg[whereFwdZeros]
	# 	newImg[whereBackZeros] = trFwdImg[whereBackZeros]
	# 	whereBoth = np.logical_and(trFwdImg != 0, trBackImg != 0)
	# 	newImg[whereBoth] = (trFwdImg[whereBoth] + trBackImg[whereBoth]) / 2
	# 	newError = np.sum(np.abs(npFrames[gen] - newImg))
	# 	whereNewZeros = np.where(newImg==0)
	# 	if fwdError < backError:
	# 		newImg2 = trFwdImg
	# 		newImg2[whereFwdZeros] = trBackImg[whereFwdZeros]
	# 	else:
	# 		newImg2 = trBackImg
	# 		newImg2[whereBackZeros] = trFwdImg[whereBackZeros]
	# 	newError2 = np.sum(np.abs(npFrames[gen] - newImg2))

	# 	newErrors.append(newError)
	# 	newErrors2.append(newError2)
	# 	newOcclusions.append(len(whereNewZeros[0]))

	# 	print("%d-%d-%d" % (start, gen, end), "Fwd error", fwdError, "Back error", \
	# 		backError, "New error", newError, "New error2", newError2)
	# 	print("Occlusions: %d %d %d" % (len(whereFwdZeros[0]), len(whereBackZeros[0]), len(whereNewZeros[0])))
	# 	newImg = np.swapaxes(np.swapaxes(np.uint8(((newImg * 0.5) + 0.5) * 255), 0, 1), 1, 2)
	# 	plot_image(newImg, axis=(1, 5, 4), new_figure=False, show_axis=False, \
	# 		title="Error: %d\nNum occlusions: %d" % (newError, len(whereNewZeros[0])))
	# 	plot_image(newImg2, axis=(1, 5, 5), new_figure=False, show_axis=False, \
	# 		title="Error: %d\nNum occlusions: %d" % (newError2, len(whereNewZeros[0])))
	# 	figure_set_size((14, 4))
	# 	plt.savefig("res/%d.png" % (i))
	# 	plt.gcf().clear()
	# 	plt.clf()

	# plt.figure()
	# plt.plot(np.arange(len(fwdErrors)), fwdErrors, label="Forward error")
	# plt.plot(np.arange(len(fwdErrors)), backErrors, label="Backward error")
	# plt.plot(np.arange(len(fwdErrors)), newErrors, label="Combined error")
	# plt.plot(np.arange(len(fwdErrors)), newErrors2, label="Combined error (#2)")
	# plt.xlabel("frame")
	# plt.ylabel("error")
	# plt.legend()
	# plt.savefig("res/combined_errors.png")

	# print(np.mean(np.array(fwdErrors)), np.mean(np.array(backErrors)), np.mean(np.array(newErrors)), np.mean(np.array(newErrors2)))

	# plt.figure()
	# plt.plot(np.arange(len(fwdOcclusions)), fwdOcclusions, label="Forward occlusions")
	# plt.plot(np.arange(len(fwdOcclusions)), backOcclusions, label="Backward occlusions")
	# plt.plot(np.arange(len(fwdOcclusions)), newOcclusions, label="Combined occlusions")
	# plt.xlabel("frame")
	# plt.ylabel("occlusions")
	# plt.legend()
	# plt.savefig("res/combined_occlusions.png")

if __name__ == '__main__':
	main()
