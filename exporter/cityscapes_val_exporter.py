import json
import h5py
import argparse
import scipy.misc
import numpy as np
from joblib import Parallel, delayed
from tqdm import tqdm
from path import Path
from lycon import resize
from PIL import Image

class cityscapes_loader(object):
	def __init__(self, dataset_dir, split='train', crop_bottom=True, img_height=171, img_width=416):
		self.dataset_dir = Path(dataset_dir)
		self.split = split
		# Crop out the bottom 25% of the image to remove the car logo
		self.crop_bottom = crop_bottom
		self.img_height = img_height
		self.img_width = img_width
		self.min_speed = 2
		self.scenes = (self.dataset_dir/'leftImg8bit_sequence'/split).dirs()
		print('Total scenes collected: {}'.format(len(self.scenes)))

	def collect_scenes(self, city):
		img_files = sorted(city.files('*.png'))
		scenes = {}
		connex_scenes = {}
		connex_scene_data_list = []
		for f in img_files:
			scene_id,frame_id = f.basename().split('_')[1:3]
			if scene_id not in scenes.keys():
				scenes[scene_id] = []
			scenes[scene_id].append(frame_id)

		# divide scenes into connexe sequences
		for scene_id in scenes.keys():
			previous = None
			connex_scenes[scene_id] = []
			for id in scenes[scene_id]:
				if previous is None or int(id) - int(previous) > 1:
					current_list = []
					connex_scenes[scene_id].append(current_list)
				current_list.append(id)
				previous = id

		# create scene data dicts, and subsample scene every two frames
		for scene_id in connex_scenes.keys():
			intrinsics = self.load_intrinsics(city, scene_id)
			for subscene in connex_scenes[scene_id]:
				frame_speeds = [self.load_speed(city, scene_id, frame_id) for frame_id in subscene]
				connex_scene_data_list.append({'city':city,
											   'scene_id': scene_id,
											   'rel_path': city.basename()+'_'+scene_id+'_'+subscene[0]+'_0',
											   'intrinsics': intrinsics,
											   'frame_ids':subscene[0::2],
											   'speeds':frame_speeds[0::2]})
				connex_scene_data_list.append({'city':city,
											   'scene_id': scene_id,
											   'rel_path': city.basename()+'_'+scene_id+'_'+subscene[0]+'_1',
											   'intrinsics': intrinsics,
											   'frame_ids': subscene[1::2],
											   'speeds': frame_speeds[1::2]})
		return connex_scene_data_list

	def load_intrinsics(self, city, scene_id):
		city_name = city.basename()
		camera_folder = self.dataset_dir/'camera'/self.split/city_name
		camera_file = camera_folder.files('{}_{}_*_camera.json'.format(city_name, scene_id))[0]
		frame_id = camera_file.split('_')[2]
		frame_path = city/'{}_{}_{}_leftImg8bit.png'.format(city_name, scene_id, frame_id)

		with open(camera_file, 'r') as f:
			camera = json.load(f)
		fx = camera['intrinsic']['fx']
		fy = camera['intrinsic']['fy']
		u0 = camera['intrinsic']['u0']
		v0 = camera['intrinsic']['v0']
		intrinsics = np.array([[fx, 0, u0],
							   [0, fy, v0],
							   [0,  0,  1]])

		img = scipy.misc.imread(frame_path)
		img = np.array(Image.open(frame_path))

		h,w,_ = img.shape
		zoom_y = self.img_height/h
		zoom_x = self.img_width/w

		intrinsics[0] *= zoom_x
		intrinsics[1] *= zoom_y
		return intrinsics

	def load_speed(self, city, scene_id, frame_id):
		city_name = city.basename()
		vehicle_folder = self.dataset_dir/'vehicle_sequence'/self.split/city_name
		vehicle_file = vehicle_folder/'{}_{}_{}_vehicle.json'.format(city_name, scene_id, frame_id)
		with open(vehicle_file, 'r') as f:
			vehicle = json.load(f)
		return vehicle['speed']

	def get_scene_imgs(self, scene_data):
		cum_speed = np.zeros(3)
		print(scene_data['city'].basename(), scene_data['scene_id'], scene_data['frame_ids'][0])
		for i,frame_id in enumerate(scene_data['frame_ids']):
			cum_speed += scene_data['speeds'][i]
			speed_mag = np.linalg.norm(cum_speed)
			if speed_mag > self.min_speed:
				city, scene_id = scene_data["city"], scene_data["scene_id"]
				img_path = city/'{}_{}_{}_leftImg8bit.png'.format(city.basename(), scene_id, frame_id)
				disp_path = img_path.replace("leftImg8bit_sequence", "disparity_sequence")
				disp_path = Path(disp_path.replace("leftImg8bit", "disparity"))
				img = self.load_image(img_path)
				disp = self.load_image(disp_path)
				yield {"img" : img, "disp" : disp, "id" : frame_id}
				cum_speed *= 0

	def load_image(self, img_file):
		if not img_file.isfile():
			return None
		img = np.array(Image.open(img_file))
		dtype = img.dtype if img.dtype == np.uint8 else np.uint16
		img = resize(img.astype(np.float32), height=self.img_height, width=self.img_width)
		img = img.astype(dtype)
		if self.crop_bottom:
			img = img[0 : int(self.img_height * 0.75)]
		return img

def getArgs():
	parser = argparse.ArgumentParser()
	parser.add_argument("dataset_dir", metavar='DIR',
						help='path to original dataset')
	parser.add_argument("--split")
	parser.add_argument("--output_path", type=str, default='dump.h5', help="Where to dump the data")
	parser.add_argument("--height", type=int, default=171, help="image height")
	parser.add_argument("--width", type=int, default=416, help="image width")
	parser.add_argument("--num-threads", type=int, default=4, help="number of threads to use")

	args = parser.parse_args()
	return args

def dump_example(args, data_loader, scene):
	scene_list = data_loader.collect_scenes(scene)
	allImgs = []
	allDisps = []
	allIntrinsics = []

	N = len(scene_list)
	for i, scene_data in enumerate(scene_list):
		intrinsics = scene_data["intrinsics"]

		sceneImgs = []
		sceneDisps = []
		for sample in data_loader.get_scene_imgs(scene_data):
			sceneImgs.append(sample["img"])
			sceneDisps.append(sample["disp"])
		if len(sceneImgs) <= 1:
			continue
		sceneImgs = np.array(sceneImgs)
		sceneDisps = np.array(sceneDisps, dtype="uint16")
		# sceneImgs = np.concatenate(sceneImgs, axis=-1)
		# sceneDisps = np.concatenate(sceneDisps, axis=-1)
		if sceneImgs.shape[0] != 15:
			continue
		print("%s: %d/%d. Len: %d. %s %s" % (scene, i, N, len(allImgs), sceneImgs.shape, sceneDisps.shape))

		allImgs.append(sceneImgs)
		allDisps.append(sceneDisps)
		allIntrinsics.append(intrinsics)
	return allImgs, allDisps, allIntrinsics


def main():
	args = getArgs()

	data_loader = cityscapes_loader(args.dataset_dir, img_height=args.height,img_width=args.width, split=args.split)
	Ns = [len(data_loader.collect_scenes(x)) for x in data_loader.scenes]
	print(Ns)

	f = h5py.File(args.output_path, "w")
	Key = "validation" if args.split == "val" else args.split
	f.create_group(Key)
	f = f[Key]

	current = 0
	allImgs = []
	allDisps = []
	allIntrinsics = []
	for i, scene in enumerate(tqdm(data_loader.scenes)):
		rgbs, depths, intrinsics = dump_example(args, data_loader, scene)
		print("Another batch done. Shape: %d %s" % (len(rgbs), rgbs[0].shape))
		allImgs.extend(rgbs)
		allDisps.extend(depths)
		allIntrinsics.extend(intrinsics)

	allImgs = np.array(allImgs)
	allDisps = np.array(allDisps, dtype="uint16")
	allIntrinsics = np.array(allIntrinsics)
	print("allImgs: %s %s" % (allImgs.shape, type(allImgs)))

	if args.split == "train":
		perm = np.random.permutation(len(allImgs))
		allImgs = allImgs[perm]
		allDisps = allDisps[perm]
		allIntrinsics = allIntrinsics[perm]

	N = len(allImgs)
	f["rgb"] = allImgs
	f["disparity"] = allDisps
	f["intrinsics"] = allIntrinsics

if __name__ == '__main__':
	main()
